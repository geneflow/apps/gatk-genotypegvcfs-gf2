GATK GenotypeGVCFs GeneFlow App
===============================

Version: 4.1.6.0-01

This GeneFlow app wraps the GATK GenotypeGVCFs tool.

Inputs
------

1. input: Combined g.vcf file.

2. reference_sequence: Directory that contains reference sequence fasta, index, and dict files.

Parameters
----------

1. output: Output directory - The name of the output directory to place the joint called vcf file.

